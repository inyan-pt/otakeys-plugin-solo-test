
exports.initializeEnvironment = function (env, success, error) {
  var target = env;
  if (!target || (target !== 'dev' && target !== 'prod')) {
    target = 'prod';
  }
  cordova.exec(success, error, 'OtaKeysPlugin', 'initializeEnvironment', [target]);
};

exports.onServiceReady = function (ready, error) {
  cordova.exec(ready, error, 'OtaKeysPlugin', 'onServiceReady', []);
};

exports.openSession = function (apiKey, secretKey, tokenKey, target, success, error) {
  var options = {
    apiKey: apiKey,
    secretKey: secretKey,
    tokenKey: tokenKey,
    target: target
  };
  cordova.exec(success, error, 'OtaKeysPlugin', 'openSession', [options]);
};

exports.closeSession = function (success, error) {
  cordova.exec(success, error, 'OtaKeysPlugin', 'closeSession', []);
};

exports.getAccessDeviceToken = function (target, success, error) {
  cordova.exec(success, error, 'OtaKeysPlugin', 'getAccessDeviceToken', [target]);
};

exports.getKeys = function (success, error) {
  cordova.exec(success, error, 'OtaKeysPlugin', 'getKeys', []);
};

exports.enableKey = function (otaId, success, error) {
  cordova.exec(success, error, 'OtaKeysPlugin', 'enableKey', [otaId]);
};

exports.switchToKey = function (otaId, success, error) {
  cordova.exec(success, error, 'OtaKeysPlugin', 'switchToKey', [otaId]);
};

exports.scanBle = function (success, error) {
  cordova.exec(success, error, 'OtaKeysPlugin', 'scanBle', []);
};

exports.stopScanningBle = function (success, error) {
  cordova.exec(success, error, 'OtaKeysPlugin', 'stopScanningBle', []);
};

exports.connectBle = function (success, error) {
  cordova.exec(success, error, 'OtaKeysPlugin', 'connectBle', []);
};

exports.disconnectBle = function (success, error) {
  cordova.exec(success, error, 'OtaKeysPlugin', 'disconnectBle', []);
};

exports.unlockDoors = function (requestVehicleData, success, error) {
  cordova.exec(success, error, 'OtaKeysPlugin', 'unlockDoors', [requestVehicleData]);
};

exports.lockDoors = function (requestVehicleData, success, error) {
  cordova.exec(success, error, 'OtaKeysPlugin', 'lockDoors', [requestVehicleData]);
};

exports.getVehicleData = function (success, error) {
  cordova.exec(success, error, 'OtaKeysPlugin', 'getVehicleData', []);
};

exports.enableEngine = function (requestVehicleData, success, error) {
  cordova.exec(success, error, 'OtaKeysPlugin', 'enableEngine', [requestVehicleData]);
};

exports.disableEngine = function (requestVehicleData, success, error) {
  cordova.exec(success, error, 'OtaKeysPlugin', 'disableEngine', [requestVehicleData]);
};

exports.isConnectedToVehicle = function (success) {
  cordova.exec(success, function (error) {
    success({
      message: JSON.stringify(error),
      connected: false
    });
  }, 'OtaKeysPlugin', 'isConnectedToVehicle', []);
};
