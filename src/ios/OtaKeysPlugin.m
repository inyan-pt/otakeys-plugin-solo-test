//
//  OtaKeysPlugin.h
//  OtaKeysMvp
//
//  Created by Ricardo Silva on 06/02/2019.
//

#import "OtaKeysPlugin.h"
#import "DataJSONObject.h"

#define OTAManager [OTAManager instance]

@interface OtaKeysPlugin () <OTABLEEventsDelegate>
@property (nonatomic) Environment *environment;
@end

@implementation OtaKeysPlugin

#pragma mark - Init

- (void)pluginInitialize {
    NSLog(@"[OtaKeysPlugin] Initialize");
    _environment = [[Environment alloc] initWithValue:kOTAEnvironmentProd];
    OTAManager.delegate = self;
}

#pragma mark Initialize

- (void)onServiceReady:(CDVInvokedUrlCommand*)command {
    NSLog(@"[OtaKeysPlugin] Service ready");
    [self.commandDelegate runInBackground:^{
        @try {
            
            NSString *authenticated = [OTAManager authenticated] ? @"true" : @"false";
            [self callbackSuccess:[DataJSONObject authenticatedObject:authenticated]
                          command:command];
        }
        @catch (NSException *exception) {
            [self callbackException:exception command:command];;
        }
    }];
}

- (void)initializeEnvironment:(CDVInvokedUrlCommand*)command {
    if ([self validateCommand:command ofClass:[NSString class]]) {
        [self.commandDelegate runInBackground:^{
            @try {
                
                NSString *environment = command.arguments.firstObject;
                _environment = [[Environment alloc] initWithValue:environment];
                
                if ([_environment isDev]) {
                    [OTAManager activateLogging:YES];
                }
                
                [self callbackSuccess:[DataJSONObject successObject:@"Environment set: "
                                                               data:[_environment value]]
                              command:command];
            }
            @catch (NSException *exception) {
                [self callbackException:exception command:command];;
            }
        }];
    }
}

#pragma mark - Session

- (void)openSession:(CDVInvokedUrlCommand*)command {
    if ([self validateCommand:command ofClass:[NSDictionary class]]) {
        [self.commandDelegate runInBackground:^{
            @try {
                
                //  Arguments
                NSDictionary* options = command.arguments.firstObject;
                NSString *apiKey = options[kOTApiKey];
                NSString *secretKey = options[kOTASecretKey];
                NSString *tokenKey = options[kOTATokenKey];
                NSString *target = options[kOTATarget];
                
                //  Environment
                OTAEnvironment environment = [Environment getOTAEnumEnvironmentFromString:target];
                [OTAManager configureEnvironment:environment];
                
                if ([_environment isDev]) {
                    //  Debugging
                    [OTAManager configureWithAppId:apiKey SDKInstanceID:secretKey];
                }
                
                //  Session
                [OTAManager openSessionWithToken:tokenKey
                                         success:^(BOOL success) {
                                             
                                             NSString *successString = success ? @"true" : @"false";
                                             // Authentication succeeded or not!
                                             [self callbackSuccess:[DataJSONObject successObject:@"Session opened: "
                                                                                            data:successString]
                                                                    command:command];
                                             
                                         }
                                         failure:^(OTAErrorCode errorCode, NSError *error) {
                                             
                                             [self callbackError:[DataJSONObject errorObject:error.description
                                                                                        code:@(errorCode)]
                                                         command:command];
                                         }];
            }
            @catch (NSException *exception) {
                [self callbackException:exception command:command];;
            }
        }];
    }
}

- (void)closeSession:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        @try {
            
            [OTAManager closeSession];
            [self callbackSuccess:[DataJSONObject successObject:@"Session closed"]
                          command:command];
            
        }
        @catch (NSException *exception) {
            [self callbackException:exception command:command];;
        }
    }];
}

#pragma mark Token

- (void)getAccessDeviceToken:(CDVInvokedUrlCommand*)command {
    if ([self validateCommand:command ofClass:[NSString class]]) {
        [self.commandDelegate runInBackground:^{
            @try {
                
                NSString *target = command.arguments.firstObject;
                
                //  Environment
                OTAEnvironment environment = [Environment getOTAEnumEnvironmentFromString:target];
                [OTAManager configureEnvironment:environment];
                
                //  Token
                NSString *accessToken = [OTAManager accessDeviceToken];
                [self callbackSuccess:[DataJSONObject tokenObject:@"Access token received"
                                                            token:accessToken]
                              command:command];
                
            }
            @catch (NSException *exception) {
                [self callbackException:exception command:command];
            }
        }];
    }
}

#pragma mark - Keys

- (void)getKeys:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        @try {
            
            [OTAManager keysWithSuccess:^(NSArray<OTAKeyPublic *> * keys) {

                NSString *otaId = kOTADefaultValue;
                if (keys.count > 0) {
                    otaId = keys.firstObject.otaId;
                }
                NSString *message = [NSString stringWithFormat:@"Keys obtained: %@",otaId];
                [self callbackSuccess:[DataJSONObject keysObject:message
                                                            keys:keys]
                              command:command];
            } failure:^(OTAErrorCode errorCode, NSError *error) {

                [self callbackError:[DataJSONObject errorObject:error.description
                                     code:@(errorCode)]
                            command:command];
            }];
        }
        @catch (NSException *exception) {
            [self callbackException:exception command:command];
        }
    }];
}

- (void)enableKey:(CDVInvokedUrlCommand*)command {
    if ([self validateCommand:command ofClass:[NSString class]]) {
        [self.commandDelegate runInBackground:^{
            @try {
                
                NSString *otaId = command.arguments.firstObject;
                OTAKeyRequestBuilder *requestBuilder = [OTAKeyRequestBuilder new];
                requestBuilder.otaId = otaId;
                OTAKeyRequest *keyRequest = [[OTAKeyRequest alloc] initWithBuilder:requestBuilder];
                [OTAManager enableKey:keyRequest completion:^(OTAKeyPublic *key, NSError *error) {

                    NSString *otaId = kOTADefaultValue;
                    if (key.otaId.length > 0) {
                        otaId = key.otaId;
                    }
                    NSString *message = [NSString stringWithFormat:@"Keys enabled: %@",otaId];
                    [self callbackSuccess:[DataJSONObject keyObject:message
                                                                key:key]
                                  command:command];
                }];
            }
            @catch (NSException *exception) {
                [self callbackException:exception command:command];
            }
        }];
    }
}

- (void)switchToKey:(CDVInvokedUrlCommand*)command {
    if ([self validateCommand:command ofClass:[NSString class]]) {
        [self.commandDelegate runInBackground:^{
            @try {
                
                NSString *otaId = command.arguments.firstObject;
                [OTAManager switchToKeyWithID:otaId completionBlock:^(BOOL success) {

                    if (success){
                        OTAKeyPublic *key = [OTAKeyPublic new];
                        key.otaId = otaId;
                        NSString *message = [NSString stringWithFormat:@"Switched to key: %@",otaId];
                        [self callbackSuccess:[DataJSONObject keyObject:message
                                                                    key:key]
                                      command:command];
                    } else {

                        NSString *message = [NSString stringWithFormat:@"switchToKey failed :%@",otaId];
                        [self callbackError:[DataJSONObject errorObject:message]
                                    command:command];
                    }
                }];
            }
            @catch (NSException *exception) {
                [self callbackException:exception command:command];
            }
        }];
    }
}

#pragma mark - Bluetooth

- (void)scanBle:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        @try {
            
            [OTAManager scanForVehicleWithCompletion:^(NSError * error) {
                
                if (error == nil) {
                    [self callbackSuccess:[DataJSONObject successObject:@"Device found"]
                                  command:command];
                } else {
                    [self callbackError:[DataJSONObject errorObject:error.description]
                                command:command];
                }
            }];
        }
        @catch (NSException *exception) {
            [self callbackException:exception command:command];
        }
    }];
}

- (void)stopScanningBle:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        @try {
            
            [OTAManager stopScanning];
            [self callbackSuccess:[DataJSONObject successObject:@"Scan stopped"]
                          command:command];
            
        }
        @catch (NSException *exception) {
            [self callbackException:exception command:command];
        }
    }];
}

- (void)connectBle:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        @try {
            
            [OTAManager connectToVehicleWithCompletion:^(NSError * error) {
                
                if (error == nil) {
                    [self callbackSuccess:[DataJSONObject successObject:@"Connect success"]
                                  command:command];
                } else {
                    [self callbackError:[DataJSONObject errorObject:error.description]
                                command:command];
                }
            }];
            
        }
        @catch (NSException *exception) {
            [self callbackException:exception command:command];
        }
    }];
}

- (void)disconnectBle:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        @try {
            
            [OTAManager disconnectFromVehicle];
            [self callbackSuccess:[DataJSONObject successObject:@"Disconnect success"]
                          command:command];
            
        }
        @catch (NSException *exception) {
            [self callbackException:exception command:command];
        }
    }];
}

#pragma mark Protocol

- (void)bluetoothStateChanged:(OTABLEConnectionStatus)connectionStatus withError:(OTABLEErrorCode)error {
    NSLog(@"[OtaKeysPlugin] - bluetoothStateChanged - ConnectionStatus  :%ld",(long)connectionStatus);
    NSLog(@"[OtaKeysPlugin] - bluetoothStateChanged - Error             :%ld",error);
}

- (void)operationPerformedWithCode:(OTAOperationCode)operationCode andState:(OTAOperationState)operationState {
    NSLog(@"[OtaKeysPlugin] - operationPerformedWithCode - OperationCode    :%ld",operationCode);
    NSLog(@"[OtaKeysPlugin] - operationPerformedWithCode - OperationState   :%ld",operationState);
}

#pragma mark - Vehicle

- (void)unlockDoors:(CDVInvokedUrlCommand*)command {
    if ([self validateCommand:command ofClass:[NSNumber class]]) {
        [self.commandDelegate runInBackground:^{
            @try {
                
                NSNumber *requestVehicleData = command.arguments.firstObject;
                [OTAManager unlockDoorsWithRequestVehicleData:requestVehicleData.boolValue
                                                      success:^(OTAVehicleData * vehicleData) {
                                                          
                                                          if (vehicleData == nil) {
                                                              NSString *successString = requestVehicleData.boolValue ? @"true" : @"false";
                                                              [self callbackSuccess:[DataJSONObject successObject:@"Unlocking doors: "
                                                                                                             data:successString]
                                                                            command:command];
                                                          }else {
                                                              [self callbackSuccess:[DataJSONObject vehicleObject:@"Received vehicle data"
                                                                                                          vehicle:vehicleData]
                                                                            command:command];
                                                          }
                                                          
                                                      } failure:^(OTAVehicleData * vehicleData, OTABLEErrorCode errorCode, NSError * error) {
                                                          
                                                          [self callbackSuccess:[DataJSONObject errorObject:error.description
                                                                                                       code:@(errorCode)]
                                                                        command:command];
                                                          
                                                      }];
            }
            @catch (NSException *exception) {
                [self callbackException:exception command:command];
            }
        }];
    }
}

- (void)lockDoors:(CDVInvokedUrlCommand*)command {
    if ([self validateCommand:command ofClass:[NSNumber class]]) {
        [self.commandDelegate runInBackground:^{
            @try {
                
                NSNumber *requestVehicleData = command.arguments.firstObject;
                [OTAManager lockDoorsWithRequestVehicleData:requestVehicleData.boolValue
                                                    success:^(OTAVehicleData *vehicleData) {
                                                        
                                                        if (vehicleData == nil) {
                                                            NSString *successString = requestVehicleData.boolValue ? @"true" : @"false";
                                                            [self callbackSuccess:[DataJSONObject successObject:@"Locked doors: "
                                                                                                           data:successString]
                                                                          command:command];
                                                        }else {
                                                            [self callbackSuccess:[DataJSONObject vehicleObject:@"Received vehicle data"
                                                                                                        vehicle:vehicleData]
                                                                          command:command];
                                                        }
                                                        
                                                    } failure:^(OTAVehicleData *vehicleData, OTABLEErrorCode errorCode, NSError *error) {
                                                        
                                                        [self callbackSuccess:[DataJSONObject errorObject:error.description
                                                                                                     code:@(errorCode)]
                                                                      command:command];
                                                    }];
            }
            @catch (NSException *exception) {
                [self callbackException:exception command:command];
            }
        }];
    }
}

- (void)getVehicleData:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        @try {
            [OTAManager vehicleDataWithSuccess:^(OTAVehicleData *vehicleData) {
                
                [self callbackSuccess:[DataJSONObject vehicleObject:@"Received vehicle data"
                                                            vehicle:vehicleData]
                              command:command];
                
            } failure:^(OTABLEErrorCode errorCode, NSError *error) {
                
                [self callbackSuccess:[DataJSONObject errorObject:error.description
                                                             code:@(errorCode)]
                              command:command];
            }];
        }
        @catch (NSException *exception) {
            [self callbackException:exception command:command];
        }
    }];
}

- (void)enableEngine:(CDVInvokedUrlCommand*)command {
    if ([self validateCommand:command ofClass:[NSNumber class]]) {
        [self.commandDelegate runInBackground:^{
            @try {
                
                NSNumber *requestVehicleData = command.arguments.firstObject;
                [OTAManager enableEngineWithRequestVehicleData:requestVehicleData.boolValue
                                                       success:^(OTAVehicleData *vehicleData) {
                                                           
                                                           [self callbackSuccess:[DataJSONObject successObject:@"Engine start success"]
                                                                         command:command];
                                                           
                                                       } failure:^(OTAVehicleData *vehicleData, OTABLEErrorCode errorCode, NSError *error) {
                                                           
                                                           [self callbackSuccess:[DataJSONObject errorObject:error.description
                                                                                                        code:@(errorCode)]
                                                                         command:command];
                                                       }];
            }
            @catch (NSException *exception) {
                [self callbackException:exception command:command];
            }
        }];
    }
}

- (void)disableEngine:(CDVInvokedUrlCommand*)command {
    if ([self validateCommand:command ofClass:[NSNumber class]]) {
        [self.commandDelegate runInBackground:^{
            @try {
                
                NSNumber *requestVehicleData = command.arguments.firstObject;
                [OTAManager disableEngineWithRequestVehicleData:requestVehicleData.boolValue
                                                        success:^(OTAVehicleData *vehicleData) {
                                                            
                                                            [self callbackSuccess:[DataJSONObject successObject:@"Engine stop success"]
                                                                          command:command];
                                                            
                                                        } failure:^(OTAVehicleData *vehicleData, OTABLEErrorCode errorCode, NSError *error) {
                                                            
                                                            [self callbackSuccess:[DataJSONObject errorObject:error.description
                                                                                                         code:@(errorCode)]
                                                                          command:command];
                                                        }];
            }
            @catch (NSException *exception) {
                [self callbackException:exception command:command];
            }
        }];
    }
}

- (void)isConnectedToVehicle:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        @try {
            
            NSString *connectedToVehicle = [OTAManager connectedToVehicle] ? @"true" : @"false";
            [self callbackSuccess:[DataJSONObject successObject:@"Connected to vehicle: "
                                                           data:connectedToVehicle]
                          command:command];
            
        }
        @catch (NSException *exception) {
            [self callbackException:exception command:command];
        }
    }];
}

#pragma mark - Callback Result

- (void)callbackSuccess:(NSDictionary *)message command:(CDVInvokedUrlCommand*)command {
    [self callbackResultWithStatus:CDVCommandStatus_OK
                           message:message
                           command:command];
}

- (void)callbackError:(NSDictionary *)message command:(CDVInvokedUrlCommand*)command {
    [self callbackResultWithStatus:CDVCommandStatus_ERROR
                           message:message
                           command:command];
}

- (void)callbackResultWithStatus:(CDVCommandStatus)status message:(NSDictionary *)message command:(CDVInvokedUrlCommand*)command {
    @try {
        
        CDVPluginResult *pluginResult = [CDVPluginResult resultWithStatus:status messageAsDictionary:message];
        NSLog(@"[OtaKeysPlugin] callbackResult - [status: %u]\n%@",status, message);
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
    @catch (NSException *exception) {
        [self callbackException:exception command:command];;
    }
}

- (void)callbackException:(NSException *)exception command:(CDVInvokedUrlCommand*)command{
    [self callbackError:[DataJSONObject errorObject:@"Exception: " data:exception]
                command:command];
}

#pragma mark - Validation

- (BOOL)validateCommand:(CDVInvokedUrlCommand*)command ofClass:(Class)aClass {
    @try {
        
        //  Arguments
        if (command.arguments.count == 0) {
            [self callbackError:[DataJSONObject errorObject:@"command.arguments.count < 0"]
                        command:command];
            return false;
        }
        //  Class
        id arguments = command.arguments.firstObject;
        if (![arguments isKindOfClass:aClass]) {
            [self callbackError:[DataJSONObject errorObject:@"Argument is not " data:aClass]
                        command:command];
            return false;
        }
        return true;
    }
    @catch (NSException *exception) {
        [self callbackException:exception command:command];;
        return false;
    }
}

@end
