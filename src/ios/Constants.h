//
//  Constants.h
//  OtaKeysMvp
//
//  Created by Ricardo Silva on 06/02/2019.
//

#ifndef Constants_h
#define Constants_h

#define NotNilString(s) s ? s : @""

#pragma mark - Configuration

static NSString * const kOTApiKey           = @"apiKey";
static NSString * const kOTASecretKey       = @"secretKey";
static NSString * const kOTATokenKey        = @"tokenKey";
static NSString * const kOTATarget          = @"target";
static NSString * const kOTAAuthenticated   = @"authenticated";
static NSString * const kOTAToken           = @"token";
static NSString * const kOTAData            = @"data";
static NSString * const kOTAMessage         = @"message";
static NSString * const kOTAKey             = @"key";
static NSString * const kOTAKeys            = @"keys";

#pragma mark - Default

static NSString * const kOTADefaultValue    = @"-1";

#pragma mark - Environment

typedef NS_ENUM(NSInteger, EnvironmentPlugin){
    DEV,
    PROD
};
static NSString * const kOTAEnvironmentDev  = @"dev";
static NSString * const kOTAEnvironmentProd = @"prod";

#pragma mark - SDK

static NSString * const kOTAKeyPublicOtaId              = @"otaId";
static NSString * const kOTAKeyPublicEnabled            = @"isEnabled";
static NSString * const kOTAKeyVehicleMileageStart      = @"mileageStart";
static NSString * const kOTAKeyVehicleMileageCurrent    = @"mileageCurrent";
static NSString * const kOTAKeyPublicVehicle            = @"vehicle";
static NSString * const kOTAVehiclePublicPlate          = @"plate";

#endif /* Constants_h */
