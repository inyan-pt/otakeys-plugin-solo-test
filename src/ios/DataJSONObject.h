//
//  DataJSONObject.h
//  OtaKeysMvp
//
//  Created by Ricardo Silva on 06/02/2019.
//

#import "OtaKeysPlugin.h"

@interface DataJSONObject : NSObject
+ (NSDictionary *)successObject:(NSString *)message;
+ (NSDictionary *)successObject:(NSString *)message data:(id)data;
+ (NSDictionary *)errorObject:(NSString *)message;
+ (NSDictionary *)errorObject:(NSString *)message data:(id)data;
+ (NSDictionary *)errorObject:(NSString *)message code:(id)code;
+ (NSDictionary *)authenticatedObject:(NSString *)authenticated;
+ (NSDictionary *)tokenObject:(NSString *)message token:(NSString *)token;
+ (NSDictionary *)keyObject:(NSString *)message key:(OTAKeyPublic *)key;
+ (NSDictionary *)keysObject:(NSString *)message keys:(NSArray<OTAKeyPublic *> *)otaKeyPublic;
+ (NSDictionary *)vehicleObject:(NSString *)message vehicle:(OTAVehicleData *)vehicle;
@end
