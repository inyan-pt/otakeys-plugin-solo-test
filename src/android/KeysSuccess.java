package io.ptech.otakeys;

import java.util.List;

import com.otakeys.sdk.service.object.response.OtaKey;

public class KeysSuccess extends JObject {

    public KeysSuccess(String message, List<OtaKey> keys) {
        this.message = message;
        this.keys = keys;
    }

    String message;

    List<OtaKey> keys;
}
