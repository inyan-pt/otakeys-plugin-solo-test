package io.ptech.otakeys;

import android.Manifest;
import android.content.pm.PackageManager;
import android.util.Log;

import com.otakeys.sdk.core.tool.OtaLogger;
import com.otakeys.sdk.service.OtaKeysService;
import com.otakeys.sdk.service.api.callback.AuthenticateCallback;
import com.otakeys.sdk.service.api.callback.EnableKeyCallback;
import com.otakeys.sdk.service.api.callback.GetKeysCallback;
import com.otakeys.sdk.service.api.enumerator.ApiCode;
import com.otakeys.sdk.service.api.enumerator.HttpStatus;
import com.otakeys.sdk.service.api.enumerator.Url;
import com.otakeys.sdk.service.ble.callback.BleConnectionCallback;
import com.otakeys.sdk.service.ble.callback.BleDisableEngineCallback;
import com.otakeys.sdk.service.ble.callback.BleDisconnectionCallback;
import com.otakeys.sdk.service.ble.callback.BleEnableEngineCallback;
import com.otakeys.sdk.service.ble.callback.BleLockDoorsCallback;
import com.otakeys.sdk.service.ble.callback.BleScanCallback;
import com.otakeys.sdk.service.ble.callback.BleUnlockDoorsCallback;
import com.otakeys.sdk.service.ble.callback.BleVehicleDataCallback;
import com.otakeys.sdk.service.ble.enumerator.BleError;
import com.otakeys.sdk.service.core.callback.ServiceStateCallback;
import com.otakeys.sdk.service.core.callback.SwitchToKeyCallback;
import com.otakeys.sdk.service.object.request.OtaKeyRequest;
import com.otakeys.sdk.service.object.request.OtaSessionRequest;
import com.otakeys.sdk.service.object.response.OtaKey;
import com.otakeys.sdk.service.object.response.OtaLastVehicleData;
import com.otakeys.sdk.service.object.response.OtaVehicleData;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * This class echoes a string called from JavaScript.
 */
@SuppressWarnings("Convert2Lambda")
public class OtaKeysPlugin extends CordovaPlugin implements ServiceStateCallback {

    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;

    private CallbackContext serviceCallbackContext;

    private CallbackContext bleCallbackContext;

    private Environment environment = Environment.PROD;

    private OtaKeysPluginApplication getOtaSdk() {
        return ((OtaKeysPluginApplication) this.cordova.getActivity().getApplication());
    }

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        OtaLogger.setDebugMode(true);
        getOtaSdk().setOnListenService(this);
    }

    @Override
    public void onServiceReady(OtaKeysService service) {
        OtaLogger.log(Log.WARN, "OtaKeysPlugin", "Service ready");
        if (serviceCallbackContext != null) {
            JSONObject result = new ServiceResult(getOtaSdk().getCore().isAuthenticated()).toObject();
            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, result);
            pluginResult.setKeepCallback(true);
            serviceCallbackContext.sendPluginResult(pluginResult);
        }
    }

    @Override
    public boolean execute(String action, final JSONArray args, final CallbackContext callbackContext) {
        if (action.equals("onServiceReady")) {
            serviceCallbackContext = callbackContext;
            return true;
        }
        if (action.equals("initializeEnvironment")) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    initializeEnvironment(args, callbackContext);
                }
            });
            return true;
        }
        if (action.equals("openSession")) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    openSession(args, callbackContext);
                }
            });
            return true;
        }
        if (action.equals("closeSession")) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    closeSession(callbackContext);
                }
            });
            return true;
        }
        if (action.equals("getAccessDeviceToken")) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    getAccessDeviceToken(args, callbackContext);
                }
            });
            return true;
        }
        if (action.equals("getKeys")) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    getKeys(callbackContext);
                }
            });
            return true;
        }
        if (action.equals("enableKey")) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    enableKey(args, callbackContext);
                }
            });
            return true;
        }
        if (action.equals("switchToKey")) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    switchToKey(args, callbackContext);
                }
            });
            return true;
        }
        if (action.equals("scanBle")) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    scanBle(callbackContext);
                }
            });
            return true;
        }
        if (action.equals("stopScanningBle")) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    stopScanningBle(callbackContext);
                }
            });
            return true;
        }
        if (action.equals("connectBle")) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    connectBle(callbackContext);
                }
            });
            return true;
        }
        if (action.equals("disconnectBle")) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    disconnectBle(callbackContext);
                }
            });
            return true;
        }
        if (action.equals("unlockDoors")) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    unlockDoors(args, callbackContext);
                }
            });
            return true;
        }
        if (action.equals("lockDoors")) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    lockDoors(args, callbackContext);
                }
            });
            return true;
        }
        if (action.equals("getVehicleData")) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    getVehicleData(callbackContext);
                }
            });
            return true;
        }
        if (action.equals("enableEngine")) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    enableEngine(args, callbackContext);
                }
            });
            return true;
        }
        if (action.equals("disableEngine")) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    disableEngine(args, callbackContext);
                }
            });
            return true;
        }
        if (action.equals("isConnectedToVehicle")) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    isConnectedToVehicle(callbackContext);
                }
            });
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) {
        boolean permissionAccepted = false;
        if (PERMISSION_REQUEST_COARSE_LOCATION == requestCode) {
            if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {
                permissionAccepted = true;
                startScanningBle(bleCallbackContext);
            }
        }
        if (serviceCallbackContext != null) {
            JSONObject result = new BluetoothResult(permissionAccepted).toObject();
            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, result);
            pluginResult.setKeepCallback(true);
            serviceCallbackContext.sendPluginResult(pluginResult);
        }
    }

    private void initializeEnvironment(JSONArray args, CallbackContext callbackContext) {
        try {
            String env = args.getString(0);
            environment = Environment.fromString(env);

            if (environment == Environment.DEV) {
                OtaLogger.setDebugMode(true);
            }
            callbackContext.success(new Success("Environment set: " + environment).toObject());
        } catch (Exception e) {
            callbackContext.error(new Error("Error: " + e.getMessage()).toObject());
        }
    }

    private void openSession(JSONArray args, final CallbackContext callbackContext) {
        try {
            JSONObject options = args.getJSONObject(0);
            String apiKey = options.getString("apiKey");
            String secretKey = options.getString("secretKey");
            String tokenKey = options.getString("tokenKey");
            String target = options.getString("target");

            getOtaSdk().getCore().configureEnvironment(Url.valueOf(target));
            if (environment == Environment.DEV) {
                getOtaSdk().getCore().debugSetAccessDevice(apiKey, secretKey);
            }

            OtaSessionRequest otaSessionRequest = new OtaSessionRequest.AccessDeviceBuilder(tokenKey).create();
            getOtaSdk().getCore().openSession(otaSessionRequest, new AuthenticateCallback() {
                @Override
                public void onAuthenticated() {
                    callbackContext.success(new Success("Session opened: " + true).toObject());
                }

                @Override
                public void onApiError(HttpStatus httpStatus, ApiCode errorCode) {
                    callbackContext.error(new Error("Error: [" + httpStatus + "]: " + errorCode).toObject());
                }
            });
        } catch (Exception e) {
            callbackContext.error(new Error("Error: " + e.getMessage()).toObject());
        }
    }

    private void closeSession(final CallbackContext callbackContext) {
        try {
            getOtaSdk().getCore().closeSession();
            callbackContext.success(new Success("Session closed").toObject());
        } catch (Exception e) {
            callbackContext.error(new Error("Error: " + e.getMessage()).toObject());
        }
    }

    private void getAccessDeviceToken(JSONArray args, CallbackContext callbackContext) {
        try {
            String target = args.getString(0);

            getOtaSdk().getCore().configureEnvironment(Url.valueOf(target));
            String accessToken = getOtaSdk().getCore().getAccessDeviceToken();
            callbackContext.success(new TokenSuccess("Access token received", accessToken).toObject());
        } catch (Exception e) {
            callbackContext.error(new Error("Error: " + e.getMessage()).toObject());
        }
    }

    private void getKeys(final CallbackContext callbackContext) {
        try {
            getOtaSdk().getApi().getKeys(new GetKeysCallback() {
                @Override
                public void onGetKeys(List<OtaKey> list) {
                    callbackContext.success(new KeysSuccess("Keys obtained", list).toObject());
                }

                @Override
                public void onApiError(HttpStatus httpStatus, ApiCode apiCode) {
                    callbackContext.error(new Error("Error: [" + httpStatus + "]: " + apiCode).toObject());
                }
            });
        } catch (Exception e) {
            callbackContext.error(new Error("Error: " + e.getMessage()).toObject());
        }
    }

    private void enableKey(JSONArray args, final CallbackContext callbackContext) {
        try {
            long otaId = args.getLong(0);

            OtaKeyRequest otaKeyRequest = new OtaKeyRequest.EnableKeyBuilder(otaId).create();
            getOtaSdk().getApi().enableKey(otaKeyRequest, new EnableKeyCallback() {
                @Override
                public void onEnableKey(OtaKey otaKey) {
                    long id = -1;
                    if (otaKey != null) {
                        id = otaKey.getOtaId();
                    }
                    callbackContext.success(new KeySuccess("Key enabled: " + id, otaKey).toObject());
                }

                @Override
                public void onApiError(HttpStatus httpStatus, ApiCode apiCode) {
                    callbackContext.error(new Error("Error: [" + httpStatus + "]: " + apiCode).toObject());
                }
            });
        } catch (Exception e) {
            callbackContext.error(new Error("Error: " + e.getMessage()).toObject());
        }
    }

    private void switchToKey(JSONArray args, final CallbackContext callbackContext) {
        try {
            long otaId = args.getLong(0);
            OtaKey otaKey = new OtaKey();
            otaKey.setOtaId(otaId);

            getOtaSdk().getCore().switchToKey(otaKey, new SwitchToKeyCallback() {
                @Override
                public void onKeySwitched(OtaKey otaKey) {
                    long id = -1;
                    if (otaKey != null) {
                        id = otaKey.getOtaId();
                    }
                    callbackContext.success(new KeySuccess("Switched to key: " + id, otaKey).toObject());
                }
            });
        } catch (Exception e) {
            callbackContext.error(new Error("Error: " + e.getMessage()).toObject());
        }
    }

    private void scanBle(final CallbackContext callbackContext) {
        if (cordova.hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION)) {
            startScanningBle(callbackContext);
        } else {
            bleCallbackContext = callbackContext;
            cordova.requestPermission(this, PERMISSION_REQUEST_COARSE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION);
        }
    }

    private void startScanningBle(CallbackContext callbackContext) {
        try {
            getOtaSdk().getBle().scan(new BleScanCallback() {
                @Override
                public void onDeviceFound() {
                    callbackContext.success(new Success("Device found").toObject());
                }

                @Override
                public void onBleError(BleError bleError) {
                    callbackContext.error(new Error("Error: " + bleError).toObject());
                }
            });
        } catch (Exception e) {
            callbackContext.error(new Error("Error: " + e.getMessage()).toObject());
        }
    }

    private void stopScanningBle(final CallbackContext callbackContext) {
        try {
            getOtaSdk().getBle().stopScanning();
            callbackContext.success(new Success("Scan stopped").toObject());
        } catch (Exception e) {
            callbackContext.error(new Error("Error: " + e.getMessage()).toObject());
        }
    }

    private void connectBle(final CallbackContext callbackContext) {
        try {
            getOtaSdk().getBle().connect(false, new BleConnectionCallback() {
                @Override
                public void onConnected() {
                    callbackContext.success(new Success("Connect success").toObject());
                }

                @Override
                public void onBleError(BleError bleError) {
                    callbackContext.error(new Error("Error: " + bleError).toObject());
                }
            });
        } catch (Exception e) {
            callbackContext.error(new Error("Error: " + e.getMessage()).toObject());
        }
    }

    private void disconnectBle(final CallbackContext callbackContext) {
        try {
            getOtaSdk().getBle().disconnect(new BleDisconnectionCallback() {
                @Override
                public void onDisconnected() {
                    callbackContext.success(new Success("Disconnect success").toObject());
                }

                @Override
                public void onBleError(BleError bleError) {
                    callbackContext.error(new Error("Error: " + bleError).toObject());
                }
            });
        } catch (Exception e) {
            callbackContext.error(new Error("Error: " + e.getMessage()).toObject());
        }
    }

    private void unlockDoors(JSONArray args, final CallbackContext callbackContext) {
        try {
            final boolean requestVehicleData = args.getBoolean(0);

            getOtaSdk().getBle().unlockDoors(requestVehicleData, new BleUnlockDoorsCallback() {
                private boolean unlocked = false;

                private boolean receivedData = false;

                @Override
                public void onUnlockingDoors() {
                    PluginResult pluginResult =
                            new PluginResult(PluginResult.Status.OK, new Success("Unlocking doors").toObject());
                    pluginResult.setKeepCallback(true);
                    callbackContext.sendPluginResult(pluginResult);
                }

                @Override
                public void onUnlockPerformed(boolean success) {
                    PluginResult pluginResult = new PluginResult(PluginResult.Status.OK,
                            new Success("Unlocked doors: " + success).toObject());
                    pluginResult.setKeepCallback(requestVehicleData && !receivedData);
                    callbackContext.sendPluginResult(pluginResult);
                    unlocked = true;
                }

                @Override
                public void onVehicleDataUpdated(OtaVehicleData otaVehicleData) {
                    PluginResult pluginResult = new PluginResult(PluginResult.Status.OK,
                            new VehicleSuccess("Received vehicle data", otaVehicleData).toObject());
                    pluginResult.setKeepCallback(!unlocked);
                    callbackContext.sendPluginResult(pluginResult);
                    receivedData = true;
                    getOtaSdk().getApi().syncVehicleData(null);
                }

                @Override
                public void onBleError(BleError bleError) {
                    callbackContext.error(new Error("Error: " + bleError).toObject());
                }
            });
        } catch (Exception e) {
            callbackContext.error(new Error("Error: " + e.getMessage()).toObject());
        }
    }

    private void lockDoors(JSONArray args, final CallbackContext callbackContext) {
        try {
            final boolean requestVehicleData = args.getBoolean(0);

            getOtaSdk().getBle().lockDoors(requestVehicleData, new BleLockDoorsCallback() {
                private boolean locked = false;

                private boolean receivedData = false;

                @Override
                public void onLockingDoors() {
                    PluginResult pluginResult =
                            new PluginResult(PluginResult.Status.OK, new Success("Locking doors").toObject());
                    pluginResult.setKeepCallback(true);
                    callbackContext.sendPluginResult(pluginResult);
                }

                @Override
                public void onLockPerformed(boolean success) {
                    PluginResult pluginResult = new PluginResult(PluginResult.Status.OK,
                            new Success("Locked doors: " + success).toObject());
                    pluginResult.setKeepCallback(requestVehicleData && !receivedData);
                    callbackContext.sendPluginResult(pluginResult);
                    locked = true;
                }

                @Override
                public void onVehicleDataUpdated(OtaVehicleData otaVehicleData) {
                    PluginResult pluginResult = new PluginResult(PluginResult.Status.OK,
                            new VehicleSuccess("Received vehicle data", otaVehicleData).toObject());
                    pluginResult.setKeepCallback(!locked);
                    callbackContext.sendPluginResult(pluginResult);
                    receivedData = true;
                    getOtaSdk().getApi().syncVehicleData(null);
                }

                @Override
                public void onBleError(BleError bleError) {
                    callbackContext.error(new Error("Error: " + bleError).toObject());
                }
            });
        } catch (Exception e) {
            callbackContext.error(new Error("Error: " + e.getMessage()).toObject());
        }
    }

    private void getVehicleData(final CallbackContext callbackContext) {
        try {
            getOtaSdk().getBle().getVehicleData(new BleVehicleDataCallback() {
                @Override
                public void onVehicleDataUpdated(OtaLastVehicleData otaVehicleData) {
                    callbackContext.success(new LastVehicleSuccess("Received vehicle data", otaVehicleData).toObject());
                }

                @Override
                public void onBleError(BleError bleError) {
                    callbackContext.error(new Error("Error: " + bleError).toObject());
                }
            });
        } catch (Exception e) {
            callbackContext.error(new Error("Error: " + e.getMessage()).toObject());
        }
    }

    private void enableEngine(JSONArray args, final CallbackContext callbackContext) {
        try {
            final boolean requestVehicleData = args.getBoolean(0);

            getOtaSdk().getBle().enableEngine(requestVehicleData, new BleEnableEngineCallback() {
                @Override
                public void onEnableEngine() {
                    callbackContext.success(new Success("Engine start success").toObject());
                }

                @Override
                public void onBleError(BleError bleError) {
                    callbackContext.error(new Error("Error: " + bleError).toObject());
                }
            });
        } catch (Exception e) {
            callbackContext.error(new Error("Error: " + e.getMessage()).toObject());
        }
    }

    private void disableEngine(JSONArray args, final CallbackContext callbackContext) {
        try {
            final boolean requestVehicleData = args.getBoolean(0);

            getOtaSdk().getBle().disableEngine(requestVehicleData, new BleDisableEngineCallback() {
                @Override
                public void onDisableEngine() {
                    callbackContext.success(new Success("Engine stop success").toObject());
                }

                @Override
                public void onBleError(BleError bleError) {
                    callbackContext.error(new Error("Error: " + bleError).toObject());
                }
            });
        } catch (Exception e) {
            callbackContext.error(new Error("Error: " + e.getMessage()).toObject());
        }
    }

    private void isConnectedToVehicle(CallbackContext callbackContext) {
        try {
            boolean connected = getOtaSdk().getBle().isConnectedToVehicle();
            callbackContext.success(new ConnectedSuccess("Connected to vehicle: " + connected, connected).toObject());
        } catch (Exception e) {
            callbackContext.success(new ConnectedSuccess("Connected to vehicle: false", false).toObject());
        }
    }
}
